Readme.txt file for Webservices coursework 1 created by sc16bmt on 11/3/2020.

This file provides a guide for usage of the web APIs in coursework 1 of sc16bmt.
APIs intended use is to provide basic rating services for students. It provides functions for user authentification, ratin and results of the rating.

This API has been tested and vetted. But for further testing purposes if you whish too access use the API, credentials have been provided.

Username: ammar

Password: 12345678Ammar

The home domain of this API is sc16bmt.pythonanywhere.com 

and its admin site is sc16bmt.pythonanywhere.com/admin/ 

With the credentials provided above you can access the admin site.

The 7 API functions prvided are:

#Register

This function registers a student using credentials provided by the student. It also checks if the username and email provided, has already been used, in order to protect database integrity.
The API is accessed at: sc16bmt.pythonanywhere.com/api/register/
Using a POST method.

#Login

This function logs a student in using credentials provided by the student. It checks if the username and password provided, is stored in the User database provided by django. It Provides authentication.
The API is accessed at: sc16bmt.pythonanywhere.com/api/login/
Using a POST method.

#Logout

This function simply logs a user out of their current session.
The API is accessed at: sc16bmt.pythonanywhere.com/api/logout/
Using a get method once the user is logged in.

#List

This function lists all the teachers and modules currently in the django database model.
This is accessed at: sc16bmt.pythonanywhere.com/api/list/
Using a get method once the user is logged in.

#View

This function also simply lists the  average rating of all teachers for every module they teach in the database.Those that havent been rated yet have a rating of 0)
This is accessed at: sc16bmt.pythonanywhere.com/api/view/
Using a get method once the user is logged in.

#Average

This function allows a student to see the average rating of a professor for a particular module.
The API is accessed at: sc16bmt.pythonanywhere.com/api/average/
Using a POST method once the user is logged in.

#Rate

This function allows a student to make a rating of a proffesser for a particular module, year and semester.
The API is accessed at: sc16bmt.pythonanywhere.com/api/rate/
Using a POST method once the user is logged in.

#Instructions


*****NOTE*****

For this code to run you will require a few things:

- Python3 
- Requests module (pip install requests)
- Pretty Tables Python module (pip install PTable)
- Django (pip install django)
- Django Rest Framework (pip install djangorestframework)
- A Virtual environment (Optional)

Make sure all of these are installed. 
******************************************************************

*****INSTRUCTIONS*****

Once all neccessary file are installed:

1) Navigate to myclient directory and enter it.

2) Once navigated to myclient directory i.e where the client.py file is, open your terminal and run the code:

- python client.py

3) This will activate the client application which will then prompt you to either register or login.

4) Since you have already been registered there is no need to but if you wish to know how, simply type "register" in the terminal once prompted to choose an option and then further entere details you have been prompted too. Should the details no satisfy the criteria an error message will be displayed explaining the criteria.

5) To login simply enter "login" then a space followed by the domain url as stated above. Then this will prompt user to enter details which have been provied.

6) Once logged in the tag should display your username above the enter options prompt.

7) To view the list function type "list".

8) To view the view function type "view".

9) To see the average of a teacher per module type in "average" followed by a space then the teacher code e.g ja1 then another space followed by the module code e.g sc.

10) To rate a teacher you simply type "rate teacher_code module_code module_year module_semester ratig_value" in that specific order where module_code, teacher_code, module_year, module_semester and rating are variables.

11) To logout, at any point when logged in type "logout".

12) Finally to quit, at any point when prompted to enter an option simply type "quit". 

*****NOTE*****
MAke sure to check terminal well.....It may be hard to see results as terminal automatically scrolls down, so ensure to scroll up after every action to see confirmation or error message.
********************************************************************************************************************************************************************************************


