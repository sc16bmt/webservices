#Imports for the client application
import json
import requests
import sys
import re
from prettytable import PrettyTable

#Reistration function
def register():
    #Create email regex
    regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
    #Promppts users for inputs of username, email, password and asks user to confrm his or her password.
    print("\n")
    a = input("Enter a username : ")
    print("\n")
    b = input("Enter your email : ")
    print("\n")
    c = input("Enter a password : ")
    print("\n")
    d = input("Confirm password : ")
    print("\n")
    #Creates server url string to send post request.
    #Change when done
    web = "sc16bmt.pythonanywhere.com"
    #web = "127.0.0.1:8000"
    loginurl = "http://" + web + "/api/register/"
    #Checks email value aginst regex
    value1 = re.search(regex,b)
    #Checks password lenght and that they confirm(match)
    value2 = c == d and (len(c)>5)
    #Confirms username is alphanumeric
    value3 = a.isalnum() and (len(a)>2)

    #If all three values are true i.e test is passed then data is sent in the PARAMS list to the server.
    if ( value1 and value2 and value3):
        PARAMS = {'uname' : a, 'email' : b, 'passrr' : c}
        r = requests.post(loginurl, data = PARAMS )
        #If the code is any of these values 200 or 201 return json string
        if r.status_code == 200 or 201:
            r = r.json()
            string = r['phrase']
            print(string)
        #Else print error string
        else:
            string = "\n\n\n\n<<<<<Error. Reistrations Failed. Invalid credentials or User does not exist. Try again or register first!!! Or try again later as server may not be available>>>>>\n\n\n\n"
            print(string)
    else:
        print("\n\n\n\n<<<<<Sorry!! Invalid values detected. Ensure :  >>>>>\n\n1) Your username is minimum length 3 and is alpha numeric.\n2) Passwords match and are also minimum length 6.\n3) Your email is valid.\n\n\n\n")


#Login client function
def login(key,url):
    #Promppts users for inputs(username and password) after taking in key variabe and address url input
    print("\n")
    a = input("Enter your username : ")
    print("\n")
    b = input("Enter your password : ")
    print("\n")
    #Creates website url from address given
    #switch back to pythonanywhere later
    web = url
    #web = "127.0.0.1:8000"
    loginurl = "http://" + web + "/api/login/"
    #Attatches parameters to request and sends request to server
    PARAMS = {'uname' : a, 'passrr' : b}
    r = requests.post(loginurl, data = PARAMS )
    #If status code is 200 i.e okay ten message. tokern kay and username are returned to client app
    if r.status_code == 200:
        r = r.json()
        string = r['phrase']
        #Return token and username
        key = r['token']
        uname = r['uname']
        print(string)
        returnlist = {'key' : key, 'uname' : uname}
        return returnlist
    #Else return error message
    elif r.status_code == 201:
        r = r.json()
        string = r['phrase']
        #Retrun empty key and username
        key = ""
        uname = ""
        print(string)
        returnlist = {'key' : key, 'uname' : uname}
        return returnlist
    #Else return error message
    else:
        string = "\n\n\n\n<<<<<Error! Login Failed. Invalid credentials or User does not exist. Try again or register first!!! Or try again later as server may not be available>>>>>\n\n\n\n"
        key = ""
        uname = ""
        returnlist = {'key' : key, 'uname' : uname}
        print(string)
        return returnlist

#Logout function
#Takes in key variable
def logout(key):
    print("\n")
    #If key variable is empty the error mssage sent back as user is not logged in.
    if key == "":
        string = "\n\n\n\n<<<<<Error! You are not logged in!!!.>>>>> \n<<<<<Log in first!!!!!!>>>>>\n\n\n\n"
        #Empty key and username sent back.
        key = ""
        uname = ""
        returnlist = {'key' : key, 'uname' : uname}
        return returnlist
    #Else sends token attatched to request and also returns empty string for key and username
    else:
        headers = {'Authorization': key}
        loginurl = 'http://sc16bmt.pythonanywhere.com/api/logout/'
        #loginurl = 'http://127.0.0.1:8000/api/logout/'
        r = requests.get(loginurl, headers = headers)
        r = r.json()
        string = r['phrase']
        key = r['token']
        uname = r['uname']
        print(string)
        returnlist = {'key' : key, 'uname' : uname}
        return returnlist

#This function lists th
def lists(key):
    print("\n")
    #Token added to request header
    loginurl = 'http://sc16bmt.pythonanywhere.com/api/list'
    #loginurl = 'http://127.0.0.1:8000/api/list'
    headers = {'Authorization': key}
    #Request sent
    r = requests.get(loginurl, headers = headers)
    #If okay is returned i.e 200 status code then table is printed
    if r.status_code == 200:
        #Data returned from request i.e response is converted to JSON.
        r = r.json()
        li = []
        #Table created and headers added
        x = PrettyTable()
        x.field_names = ["Module ID", "Module name", "Semester", "Year","Teacher ID"]
        #Data is parsed and looped.
        for i in r['phrase']:
            li.append(i)
        y = len(li)
        for i in range(y):
            #Rows of the table added from parsed data.
            x.add_row([str(li[i]['ID']),str(li[i]['name']), str(li[i]['sem']), str(li[i]['year']), str(li[i]['tc'])])
        print(x)
    #Else you are not logged in error message displayed
    else:
        string = "\n\n\n\n<<<<<Error! You are not logged in!!!.>>>>> \n<<<<<Log in first!!!!!!>>>>>\n\n\n\n"
        print(string)

#View function to display averages to client of lecturers in all modules.
def view(key):
    print("\n")
    headers = {'Authorization': key}
    loginurl = 'http://sc16bmt.pythonanywhere.com/api/view/'
    #loginurl = 'http://127.0.0.1:8000/api/view/'
    r = requests.get(loginurl, headers = headers)
    if r.status_code == 200:
        #Gets returned list and in json format and prints it.
        r = r.json()
        #Table created and headers added
        x = PrettyTable()
        x.field_names = ["Ratings"]
        li = []
        for i in r['phrase']:
            li.append(i)
        y = len(li)
        for i in range(y):
            x.add_row(["The rating of Professor "+ li[i]['name'] +" is " + str(li[i]['Rating'])])
        print(x)
    else:
        string = "\n\n\n\n<<<<<Error! You are not logged in!!!.>>>>>\n<<<<<Log in first!!!!!!>>>>>\n\n\n\n"
        print(string)

#Average function
def average(key, inp):
    print("\n")
    headers = {'Authorization': key}
    loginurl = 'http://sc16bmt.pythonanywhere.com/api/average/'
    #loginurl = 'http://127.0.0.1:8000/api/average/'
    data = {"teach_ID" : inp.split(" ")[1], "mod_ID" : inp.split(" ")[2]}
    r = requests.get(loginurl, headers = headers, data = data)
    if r.status_code == 200:
        #JSON reurns ratings
        r = r.json()
        x = PrettyTable()
        x.field_names = ["Rating"]
        x.add_row(["The rating of Professor " + r["phrase"]['name'] + " in module " + r["phrase"]['module_n'] + " " + r["phrase"]['modid'] + " is " +  str(r["phrase"]['Rating'])])
        print(x)
    #Error messages
    elif r.status_code == 403:
        print("\n\n\n\n<<<<<Error! You are not logged in!!!.>>>>>\n<<<<<Log in first!!!!!!>>>>>\n\n\n\n")
    else:
        r = r.json()
        print(r["phrase"])

#Rate function
def rate(key, inp):
    print("\n")
    headers = {'Authorization': key}
    loginurl = 'http://sc16bmt.pythonanywhere.com/api/rate/'
    #loginurl = 'http://127.0.0.1:8000/api/rate/'
    data = {"teach_ID" : inp.split(" ")[1], "mod_ID" : inp.split(" ")[2], "year" : inp.split(" ")[3], "semester" : inp.split(" ")[4], "rate" : inp.split(" ")[5]}
    r = requests.post(loginurl, headers = headers, data = data)
    if r.status_code == 200:
        r = r.json()
        print(r["phrase"])
    elif r.status_code == 403:
        print("\n\n\n\n<<<<<Error! You are not logged in!!!.>>>>>\n<<<<<Log in first!!!!!!>>>>>\n\n\n\n")
    else:
        r = r.json()
        print(r["phrase"])

#Main function of the client application
def main(args=None):
    #Initialise token key variable to b a empty string and user username to be an empty string
    key = ""
    uname = ""
    #A printed introduction detailing how to use function of the web app
    print("\n\n________________________________________________________________________________________\n\n")
    print("\nWelcome to the student's rate app. This app permits you to rate your teachers.\nHere are a list of options below:\n")
    print("\n1) register (Enter it as \"register\")\n")
    print("\n2) login (Enter it as \"login sc16bmt.pythonanywhere.com\")\n")
    print("\n3) list (Enter it as \"list\")\n")
    print("\n4) rate (Enter it as \"rate professor_id module_code year semester rating \", where rating is between 1-5)\n")
    print("\n5) view (Enter it as \"view\")\n")
    print("\n6) average (Enter it as \"average professor_id module_code\")\n")
    print("\n7) logout\n")
    print("\n8) quit\n")
    print("\n\n________________________________________________________________________________________\n\n")
    #While True variable permanently loops. When user is tired quit function is called to break out of the loop.
    while True:
        #Checks if token has been assigned by the server. If the lenght is 0 then it has not been assigned so different options are presented
        if len(key) == 0:
            #Only register, login and quite function are useable wile the user has not logged in.This is because token or key is not assigned by server until login.
            print("\n*****Your current options are:*****\n\n")
            print("\n1) register (Enter it as \"register\")\n")
            print("\n2) login (Enter it as \"login sc16bmt.pythonanywhere.com\")\n")
            print("\n3) quit\n")
            #Function demands input of a command
            inp = input("\n\n(not logged in)\nEnter an option : \n\n")
            #Checks the input is the word register
            if inp.lower() == "register":
                register()
            #Checks the first input word is login and the next is the url provided
            elif inp[0:5].lower() == "login" and len(inp.split(" ")) == 2 and inp.split(" ")[0] == "login" and inp.split(" ")[1] == "sc16bmt.pythonanywhere.com":
                l = login(key,inp.split(" ")[1])
                #Returns key and username
                key = l['key']
                uname = l['uname']
            elif inp.lower() == "quit":
                break
            #Else it reutns an invalid option message
            else:
                print("\n\n\n\n<<<<<An invalid option has been presented. Please try again and watch for spelling errors.>>>>>\n\n\n\n")
        #If the key length is greater than 0 i.e it has been sent by the server and recieved
        else:
            #A whole new set of options are provided where by the user can access list, view, average, rate logout and quit
            print("\n*****Your current options are:*****\n\n")
            print("\n1) list (Enter it as \"list\")\n")
            print("\n2) rate (Enter it as \"rate professor_id module_code year semester rating \", where rating is between 1-5)\n")
            print("\n3) view (Enter it as \"view\")\n")
            print("\n4) average (Enter it as \"average professor_id module_code\")\n")
            print("\n5) logout\n")
            print("\n6) quit\n")
            #Prompts user for input
            inp = input("\n\n("+ uname +")\nEnter an option : \n\n")
            #Checks the input word
            if inp.lower() == "list":
                #Runs list function if the input is list
                lists(key)
            #Checks the input word
            elif inp.lower() == "logout":
                l = logout(key)
                key = l['key']
                uname = l['uname']
            #Checks the input word and rate criteria. i.e that the first word is rate and that there are 6 different options provided.
            elif inp[0:4].lower() == "rate" and inp.split(" ")[0] == "rate" and len(inp.split(" ")) == 6 and (inp.split(" ")[3] == "2017" or inp.split(" ")[3] == "2018" or inp.split(" ")[3] == "2019")and (inp.split(" ")[4] == "1" or inp.split(" ")[4] == "2") and (int(inp.split(" ")[5]) >= 1 and int(inp.split(" ")[5])<= 5):
                #If checks pass the function is called
                rate(key,inp)
            elif inp.lower() == "view":
                view(key)
            elif inp[0:7].lower() == "average" and inp.split(" ")[0] == "average" and len(inp.split(" ")) == 3 :
                    average(key,inp)
            elif inp.lower() == "quit":
                break
            else:
                print("\n\n\n\n<<<<<Invalid optionn>>>>>\n\n\n\n")

#Main function called
if __name__ == '__main__':
    main()
