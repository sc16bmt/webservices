from django.db import models
from django.contrib.auth import models as md

# Create your models here.

#Teacher model
class Teacher(models.Model):
    t_ID = models.CharField('Teacher ID', max_length=3, unique= True)
    t_name = models.CharField('Teacher First Name', max_length=120)
    t_last_Name = models.CharField('Teacher Last Name', max_length=120)
    #Creates unique constrain for a name and last name. SO both are unique
    def __str__(self):
        return u'%s %s' % (self.t_name, self.t_last_Name)
#Module code
class Module(models.Model):
    Semesters = [(1,1),(2,2)]
    module_ID = models.CharField('Module ID', max_length=2)
    name = models.CharField('Module Name', max_length=120)
    description = models.TextField(blank=True)
    semester = models.IntegerField('Semester', choices = Semesters)
    year = models.DateField("Year")
    teachers = models.ManyToManyField(Teacher)
    #Module ID, name, year and semester are unique together
    class Meta:
        unique_together = ('module_ID','name', 'semester', 'year')

    def __str__(self):
        return u'%s %s' % (self.name,self.year.year)
#Ratings model
class Rating(models.Model):
    module = models.ForeignKey(Module,on_delete=models.PROTECT)
    teacher = models.ForeignKey(Teacher,on_delete=models.PROTECT)
    Rating = models.IntegerField()

    def __str__(self):
        return u'%s,    %s,  Score : %d' % (self.module,self.teacher, self.Rating)
