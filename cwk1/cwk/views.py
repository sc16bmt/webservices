#Imports for server
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import login as auth_login
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from . import models
import json
from decimal import *
import datetime




# Create your views here.
#For view function for registration linked to api/register function
#Prevents the error of midding csrf coockies.
@csrf_exempt
#Allows only post requests
@api_view(['POST'])
#Any node can access the register website
@permission_classes((AllowAny,))
def register(request):
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['Content-Type'] = 'text/plain'
    #GET messages rejected with sever sending user bad response.
    if(request.method == 'GET'):
        http_bad_response.content = 'Only POST requests are allowed for this resource\n'
        return http_bad_response
    #If the requested is POST however then the username,email, and password parameters are collected from the request object
    username = request.POST.get('uname')
    email = request.POST.get('email')
    password = request.POST.get('passrr')
    x = True
    y = True
    #User objects with the given username are counted
    z = User.objects.filter(username = username).count()
    #User objects with the given email are counted
    a = User.objects.filter(email = email).count()
    #If either count is greater than 1 i.e a record in the database exists with those credentials both boolean flags are set to false
    if z > 0:
        x = False
    if a > 0:
        y = False
    #If any of the flags are false then a failed registration message is sent back otherwise it is a success.
    if x == True and y == True:
        #User object created in database and saved.
        users = User.objects.create_user(username,email,password)
        users.save()
        payload  = {'phrase':"\n\n\n\n*****Registration successful!!!*****\n\n\n\n"}
        http_response = HttpResponse(json.dumps(payload))
        http_response['Content-Type'] = 'application/json'
        http_response.status_code= 200
        http_response.reason_phrase = 'OK'
        return http_response
    else:
        payload  = {'phrase':"\n\n\n\n<<<<<Registration Failed!!!>>>>>\n<<<<<Error: Username or email already exists>>>>>\n\n\n\n"}
        http_response = HttpResponse(json.dumps(payload))
        http_response['Content-Type'] = 'application/json'
        http_response.status_code= 201
        http_response.reason_phrase = 'Invalid Credentials'
        return http_response

#Login function
@csrf_exempt
#Allows only post requests by any authorized user.
@api_view(['POST'])
@permission_classes((AllowAny,))
def login(request):
    #Returns bad response if a get request is sent
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['Content-Type'] = 'text/plain'

    if(request.method == 'GET'):
        http_bad_response.content = 'Only POST requests are allowed for this resource\n'
        return http_bad_response
    #Collects username and password
    uname = request.POST.get('uname')
    password = request.POST.get('passrr')
    #Uses authentication function to check user with username and password and to check they match. If the user exists the function returns the user.
    user = authenticate(request, username = uname, password = password)
    #If the user doesnt exist the function returns an empty key and username field and an error message. But if he exists
    if user is not None:
        #Functions authorise the user and create a token which is then stored.
        auth_login(request, user, user.backend)
        token, _ = Token.objects.get_or_create(user=user)
        payload  = {'phrase':"\n\n\n\n*****You are now logged in!!!*****\n\n\n\n",'token': "Token " + token.key, 'uname': uname}
        http_response = HttpResponse(json.dumps(payload))
        http_response['Content-Type'] = 'application/json'
        http_response.status_code= 200
        http_response.reason_phrase = 'OK'
        return http_response
    else:
        payload  = {'phrase':"\n\n\n\n<<<<<Error. Login Failed. Invalid credentials or User does not exist. Try again or register first!!!>>>>>\n\n\n\n"}
        http_response = HttpResponse(json.dumps(payload))
        http_response['Content-Type'] = 'application/json'
        http_response.status_code= 201
        http_response.reason_phrase = 'Invalid credentials'
        return http_response



#For logout
#For list of modules and teachers and years and semesters
#Only GET methods allowed for requests
@api_view(['GET'])
#User must however be authenticated.
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated,))
def logout(request):
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['Content-Type'] = 'text/plain'
    #Post method returns bad response errors.
    if(request.method != 'GET'):
        http_bad_response.content = 'Only GET requests are allowed for this resource\n'
        return http_bad_response
    #Token made empty along with userame as json object is sent back with message.
    token = ""
    payload  = {'phrase':"\n\n\n\n*****You are now logged out!!!*****\n\n\n\n",'token': token, 'uname': ""}
    http_response = HttpResponse(json.dumps(payload))
    http_response['Content-Type'] = 'application/json'
    http_response.status_code= 200
    http_response.reason_phrase = 'OK'
    return http_response

#For list of modules and teachers and years and semesters
@api_view(['GET'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated,))
def list(request):
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['Content-Type'] = 'text/plain'

    if(request.method != 'GET'):
        http_bad_response.content = 'Only GET requests are allowed for this resource\n'
        return http_bad_response
    #Creates a list of all odules
    module_list = models.Module.objects.all().values('module_ID','name','semester','year','teachers')
    the_list = []
    #Loops through all modules and gets details of the module incuding the teachers and appends them to the list being created
    for r in module_list:
        tname = models.Teacher.objects.get(id = r['teachers'])
        string = str(tname.t_ID) + ", " + str(tname.t_name)[0]+ "." + str(tname.t_last_Name)
        item = {'ID':r['module_ID'],'name': r['name'],'sem': r['semester'], 'year': r['year'].strftime("%Y"),'tc': string}
        the_list.append(item)
    #Sends the list back to the client
    payload  = {'phrase':the_list}
    http_response = HttpResponse(json.dumps(payload))
    http_response['Content-Type'] = 'application/json'
    http_response.status_code= 200
    http_response.reason_phrase = 'OK'
    return http_response

#For view
@api_view(['GET'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated,))
def view(request):
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['Content-Type'] = 'text/plain'

    if(request.method != 'GET'):
        http_bad_response.content = 'Only GET requests are allowed for this resource\n'
        return http_bad_response

    #List of all teachers returned
    teacher_list = models.Teacher.objects.all()
    the_list = []
    #Loops through teachers list
    for i in teacher_list:
        #Sets ratting sum and rating average to 0
        ratingsum = 0
        ratingaverage = 0
        #Gets the list of Ratings concerning a particular teacher
        trlist = models.Rating.objects.filter(teacher = i.id )
        #Counts the teacher rating list
        trcount = models.Rating.objects.filter(teacher = i.id ).count()
        for j in trlist:
            #For each rating in the list it adds it to the sum of ratings
            ratingsum = ratingsum + j.Rating
        #If this sum is greater than 0 and so is the number of ratings, it returns the average by dividing by the count
        if ratingsum > 0 and trcount > 0:
            #Rounding
            ratingaverage = int(Decimal(ratingsum/trcount).quantize(Decimal('1.'),rounding = ROUND_HALF_UP))
        #Else the rating is 0 as the teacher has not been rated yet
        else:
            ratingaverage = 0
        #Adds results to the list and returns the list to the client
        name = i.t_name[0] + "." + i.t_last_Name
        item = {'Rating':ratingaverage,'name': name}
        the_list.append(item)
    payload  = {'phrase':the_list}
    http_response = HttpResponse(json.dumps(payload))
    http_response['Content-Type'] = 'application/json'
    http_response.status_code= 200
    http_response.reason_phrase = 'OK'
    return http_response

#For average function
@api_view(['GET'])
#Access must be authenticated
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated,))
def average(request):
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['Content-Type'] = 'text/plain'

    if(request.method != 'GET'):
        http_bad_response.content = 'Only GET requests are allowed for this resource\n'
        return http_bad_response
    #Uses id input to get the teacher  ID and MOdule ID
    tid = request.POST.get('teach_ID').upper()
    mid = request.POST.get('mod_ID').upper()
    #Finds teacher with said ID and module with said ID and counts the number of instances
    teacher = models.Teacher.objects.filter(t_ID = tid).count()
    module = models.Module.objects.filter(module_ID = mid ).count()
    #If teacher and module count is 0 then teacher or module do not exist thus it is an invalid option and an error message is sent.
    if teacher == 0 or module == 0:
        the_list = "\n\n\n\n<<<<<Invalid option>>>>>\n\n\n\n"
        payload  = {'phrase':the_list}
        http_response = HttpResponse(json.dumps(payload))
        http_response['Content-Type'] = 'application/json'
        http_response.status_code= 401
        http_response.reason_phrase = 'Invalid Details'
        return http_response
    #Else the first instance of the module is chose and the teacher is selected.
    else:
        module = models.Module.objects.filter(module_ID = mid)[0]
        teacher = models.Teacher.objects.get(t_ID = tid)
        #Rating sum and average set to 0.
        ratingsum = 0
        ratingaverage = 0
        #Rating objects with that specific teacher and module filtered.
        rtm = models.Rating.objects.filter(module = module, teacher = teacher.id)
        #Coun the number of the specific module instances with that specific teacher
        mtc = models.Module.objects.filter(module_ID = mid,teachers = teacher.id).count()
        #Count the number of ratings with that teacher and mdule selected.
        rtmcount = models.Rating.objects.filter(module = module, teacher = teacher.id).count()
        #If there exits a module with that teacher i.e mtc is greater than 0
        if mtc > 0:
            #For each rating og that teacher and that module add the rating t the rating sum then divide by the number of ratings for the average.
            for j in rtm:
                ratingsum = ratingsum + j.Rating
            #If rating usm is greater than 0
            if ratingsum > 0:
                #Rating average rounded up
                ratingaverage = int(Decimal(ratingsum/rtmcount).quantize(Decimal('1.'),rounding = ROUND_HALF_UP))
            else:
                ratingaverage = 0
            name = teacher.t_name[0] + "." + teacher.t_last_Name
            modulename = module.name
            modid = module.module_ID
            #Rating average is sent back to the cient.
            item = {'Rating':ratingaverage,'name': name, 'module_n': modulename, 'modid' : modid}
            payload  = {'phrase':item}
            http_response = HttpResponse(json.dumps(payload))
            http_response['Content-Type'] = 'application/json'
            http_response.status_code= 200
            http_response.reason_phrase = 'OK'
            return http_response

        else:
            the_list = "\n\n\n\n<<<<<Teacher does not take this module.>>>>>\n\n\n\n"
            payload  = {'phrase':the_list}
            http_response = HttpResponse(json.dumps(payload))
            http_response['Content-Type'] = 'application/json'
            http_response.status_code= 401
            http_response.reason_phrase = 'Invalid Details'
            return http_response


#For rate
@api_view(['POST'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated,))
def rate(request):
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['Content-Type'] = 'text/plain'

    if(request.method == 'GET'):
        http_bad_response.content = 'Only POST requests are allowed for this resource\n'
        return http_bad_response

    tid = request.POST.get('teach_ID').upper()
    mid = request.POST.get('mod_ID').upper()
    year = request.POST.get('year')
    sem = request.POST.get('semester')
    rate = request.POST.get('rate')

    teacher = models.Teacher.objects.filter(t_ID = tid).count()
    module = models.Module.objects.filter(module_ID = mid ).count()
    is_int = isinstance(rate, int)

    if teacher == 0 or module == 0 :
        the_list = "\n\n\n\n<<<<<Invalid option>>>>>\n\n\n\n"
        payload  = {'phrase':the_list}
        http_response = HttpResponse(json.dumps(payload))
        http_response['Content-Type'] = 'application/json'
        http_response.status_code= 401
        http_response.reason_phrase = 'Invalid Details'
        return http_response
    else:
        teacher = models.Teacher.objects.get(t_ID = tid)
        module = models.Module.objects.filter(module_ID = mid )[0]
        yr = datetime.datetime(int(year), 9, 1)
        mtc = models.Module.objects.filter(module_ID = mid,teachers = teacher.id, year = yr, semester = int(sem) ).count()
        if mtc > 0:
            the_list = "\n\n\n\n*****Rate successful*****\n\n\n\n"
            payload  = {'phrase':the_list}
            http_response = HttpResponse(json.dumps(payload))
            http_response['Content-Type'] = 'application/json'
            http_response.status_code= 200
            http_response.reason_phrase = 'OK'
            rating = models.Rating.objects.create(module = module,teacher = teacher,Rating = rate)
            rating.save()
        else:
            the_list = "\n\n\n\n<<<<<Teacher does not take this module at specified time.>>>>>\n\n\n\n"
            payload  = {'phrase':the_list}
            http_response = HttpResponse(json.dumps(payload))
            http_response['Content-Type'] = 'application/json'
            http_response.status_code= 401
            http_response.reason_phrase = 'Invalid Details'
            return http_response
        return http_response
