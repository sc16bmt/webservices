# Generated by Django 3.0.3 on 2020-02-22 00:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cwk', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='module',
            name='module_ID',
            field=models.CharField(max_length=3, verbose_name='Module ID'),
        ),
        migrations.AlterField(
            model_name='module',
            name='name',
            field=models.CharField(max_length=120, verbose_name='Module Name'),
        ),
    ]
