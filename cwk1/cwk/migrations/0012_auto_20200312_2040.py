# Generated by Django 3.0.3 on 2020-03-12 20:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cwk', '0011_auto_20200312_2036'),
    ]

    operations = [
        migrations.AlterField(
            model_name='module',
            name='semester',
            field=models.IntegerField(choices=[(1, 1), (2, 2)], verbose_name='Semester'),
        ),
    ]
